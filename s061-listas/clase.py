#Definicion de listas
notas = [10, 12, 15, 11]
promedios = [10.5, 11,4, 16.7]
nombre = ["Carlos", "Alejandra", "Cesar"]
alumnos = [200, "Juan Perez", 18.5]


#Lectura de datos
def leer_valores():
    valor = -1
    lista = []
    while(valor != 0):
        valor = int(input("Ingrese valor positivo o cero para terminar: "))
        lista.append(valor)
    return lista

lista = [2, 3, 5]
print(lista[0])
print(lista[2])