#Variables
#Enteros
a = 12
#Numeros reales (parte decimal)
b = 10.5
#Cadenas
c = "ABC"
print("Valor de a:", a)

#Operaciones matematicas
base = 13
#Incrementar el valor de base en 1
base = base + 1
altura = 7
#Calculo del area del triangulo
area = base * altura / 2
print("Valor del area:", area, "unidades")

#Ejemplo: Calcule el perimetro de un triangulo de
#  lados a, b y c y muestrelo en pantalla
a = 12
b = 15
c = 18
perimetro = a + b + c
print("El perimetro es:", perimetro)

#Calcule e imprima en pantalla el ultimo digito de n (3 cifras)
#contando desde la izquierda
n = 678 
#Operador modulo
residuo = n % 10
print(residuo)
#Division entera
cociente = n // 10
print(cociente)

#Potencia
base = 5
exponente = 3
potencia = base ** exponente
print(potencia)

def lectura_datos1():
    #Lectura de datos
    n = int(input("Ingrese valor de n: "))
    #Calcule la suma de cifras de n (3 cifras)
    unidad = n % 10
    centena = n // 100
    decena = (n // 10) % 10
    suma = unidad + decena + centena
    print("Suma de digitos:", suma)

def lectura_datos2():
    #Implemente un programa que lea el largo y el ancho de un
    #rectangulo y muestre en pantalla el valor de su perimetro
    #y area

    largo = float(input("Ingrese largo del rectangulo: "))
    ancho = float(input("Ingrese ancho del rectangulo: "))

    perimetro = 2 * (largo + ancho)
    area = largo * ancho
    print("Perimetro:", perimetro)
    print("Area:", area)
