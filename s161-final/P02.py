def obtener_matriz(filas, columnas):
    res = []
    for _ in range(filas):
            res.append([0] * columnas)
    return res
    

def agregar(matriz, lista):    
    for i in range(0, len(lista), 3):
        matriz[lista[i]][lista[i + 1]] += lista[i + 2]    

def convertir_matriz(matriz):
    res = []
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            if(matriz[i][j] != 0):
                res.append(i)
                res.append(j)
                res.append(matriz[i][j])
    return res

def sumar_listas(A, B, M, N):
    matriz = obtener_matriz(M, N)
    agregar(matriz, A)
    agregar(matriz, B)    
    return convertir_matriz(matriz)

if __name__ == "__main__":
    lista = [0, 0, 4, 2, 1, 3]
    lista_res = sumar_listas(lista, lista, 3, 3)
    print(lista_res)
    
    
    