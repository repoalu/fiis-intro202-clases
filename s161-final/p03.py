def cumple_condicion(cadena):
    return cumple_condicion_aux(cadena, 0)

def cumple_condicion_aux(cadena, conteo):
    if(conteo > 1):
        return False
    elif(len(cadena) <= 1):
        return True
    else:
        if(cadena[0] != cadena[len(cadena) - 1]):
            conteo = conteo + 1
        return cumple_condicion_aux(cadena[1 : len(cadena) - 1], conteo)

if __name__ == "__main__":
    print(cumple_condicion("123321"))
    print(cumple_condicion("123320"))
    print(cumple_condicion("123318"))