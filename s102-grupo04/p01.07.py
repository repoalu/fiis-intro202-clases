def dividir(cadena):
    lista = []
    palabra = ""
    for i in range(len(cadena)):        
        letra = cadena[i]
        #Si no es un espacio en blanco, la letra leida forma parte de una palabra
        if(cadena[i] != " "):
            palabra = palabra + letra    
        else:
            #Si encuentro un espacio en blanco - palabra terminada
            lista.append(palabra)
            palabra = ""
    lista.append(palabra)
    return lista


if __name__ == "__main__":
    cadena = "Los alumnos programan en Python"    
    lista = dividir(cadena)
    #["los", "alumnos", "programan"]
    print(lista)