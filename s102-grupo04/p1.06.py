import funciones as f

def obtener_palindromo(cadena):
    #Obtenemos el palindromo mas grande, tomando subcadenas desde la derecha
    #Las palabras de una letra e consideran palindromas, evaluamos a partir de 2
    idx = len(cadena) - 1
    for i in range(len(cadena) - 2, -1, -1):
        subcad = cadena[i:] #Desde el caracter en posicion "i" hasta el final
        if(f.es_palindromo(subcad) == True):
            idx = i
    #Obtenemos la parte no-palindroma de la cadena
    #Desde el comienzo hasta posicion "idx" (no la incluye)
    no_pal = cadena[:idx]
    return cadena + f.invertir(no_pal) 

if __name__ == "__main__":
    cadena = "MARIANA"
    palindromo = obtener_palindromo(cadena)
    print("Cadena:", palindromo)
