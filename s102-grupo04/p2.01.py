def imprimir(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            print(matriz[i][j], end = " ")
        print()
if __name__ == "__main__":
    matriz = [[1, 1, 2, 5], [3, 5, 6, 0], [9, 6, 8, 3]]
    imprimir(matriz)
        