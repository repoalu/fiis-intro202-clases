def mostrar_mejor_producto(ventas):
    ventas_max = 0
    indice_max = 0
    for i in range(len(ventas)):
        ventas_prod = 0
        for j in range(len(ventas[0])):
            ventas_prod += ventas[i][j]
        
        if(ventas_prod > ventas_max):
            ventas_max = ventas_prod
            indice_max = i + 1
    print("El producto mas vendido es:", indice_max)            
    print("Ventas totales:", ventas_max)

def mostrar_mejor_trimestre(ventas):
    ventas_max = 0
    indice_max = 0
    for j in range(len(ventas[0])):
        total_trim = 0
        for i in range(len(ventas)):
            total_trim += ventas[i][j]
        if(total_trim > ventas_max):
            ventas_max = total_trim
            indice_max = j + 1
    print("Trimestre con mayores ventas:", indice_max)
    print("Ventas:", ventas_max)

if __name__ == "__main__":
    M = [
            [1000, 1200, 3400, 5600],
            [2300, 1800, 5400, 9200],
            [1100, 1500, 2800, 3900]
        ]
    mostrar_mejor_producto(M)
    mostrar_mejor_trimestre(M)