def contar_disponibles(asientos):
    disponibles = 0
    for i in range(len(asientos)):
        for j in range(len(asientos[0])):
            if(asientos[i][j] == "0"):
                disponibles = disponibles + 1
    return disponibles        

if __name__ == "__main__":
    asientos =  [   ["X", "0", "X", "0"],
                    ["X", "X", "X", "0"],
                    ["0", "0", "X", "X"]
                ]
    res= contar_disponibles(asientos)
    print("Cantidad de asientos disponibles:", res)