def obtener_caracter(cadena):
    res = None
    #Por cada caracter de la cadena
    for i in range(len(cadena)):
        caracter = cadena[i]
        #Contamos la cantidad de apariciones
        apariciones = 0
        for j in range(len(cadena)):
            if(caracter == cadena[j]):
                apariciones = apariciones + 1
        if(apariciones == 1):
            res = caracter
            break
    return res

if __name__ == "__main__":
    cadena = input("Ingrese un texto: ")
    res = obtener_caracter(cadena)
    if(res != None):
        print("Primer caracter que no se repite:", res)
    else:
        print("Todos los caracterese se repiten")