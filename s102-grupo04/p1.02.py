def obtener_ventas(precios, unidades):
    lista = []
    ventas_max = 0
    indice_max = 0
    for i in range(len(unidades)):
        ventas = 1.18 * (precios[i] * unidades[i])
        lista.append(ventas)
        if(ventas > ventas_max):
            ventas_max = ventas
            indice_max = i
    print("Producto con mayores ventas: ", indice_max + 1)
    print("Volumen de ventas: ", ventas_max)
    return lista

if __name__ == "__main__":
    precios = [10, 5, 2.5, 6]
    unidades = [5, 2, 4, 10]
    ventas = obtener_ventas(precios, unidades)
    print(ventas)