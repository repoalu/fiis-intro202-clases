def es_palindromo(cadena):
    palindromo = True
    ini = 0
    fin = len(cadena) - 1
    while(ini <= fin):
        if(cadena[ini] != cadena[fin]):
            palindromo = False
            break
        ini += 1
        fin -= 1
    return palindromo

def invertir(cadena):
    res = ""
    #Recorremos la cadena el reves: len(cadena) - 1, len(cadena) - 2, ..., 2, 1, 0
    for i in range(len(cadena) - 1, -1, -1):
        res = res + cadena[i]
    return res