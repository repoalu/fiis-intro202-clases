def obtener_mas_altas(lista):
    mayor = 0
    segundo_mayor = 0
    for i in range(len(lista)):
        if(lista[i] > mayor):
            segundo_mayor = mayor
            mayor = lista[i]
        elif(lista[i] > segundo_mayor):
            segundo_mayor = lista[i]
    return [mayor, segundo_mayor]

def leer_datos():
    lista = []
    while(True):
        a = float(input("Ingrese valor: "))
        if(a >= 0):
            lista.append(a)
        else:
            break
    return lista

if __name__ == "__main__":
    lista = leer_datos()
    print(obtener_mas_altas(lista))