def es_simetrica(M):
    res = True
    #Matriz debe ser cuadrada
    if(len(M) != len(M[0])):
        res = False
    else:
        #filas
        for i in range(len(M)):
            #columnas
            for j in range(len(M[0])):
                if(M[i][j] != M[j][i]):
                    res = False
                    break
    return res

if __name__ == "__main__":
    M = [ [1, 2, 4],
          [2, 8, 7],
          [4, 7, 1]
        ]  
    res = es_simetrica(M)
    print(res)