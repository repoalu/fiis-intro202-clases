def mostrar_disponibilidad(asientos):
    for i in range(len(asientos)):
        for j in range(len(asientos[0])):
            if(asientos[i][j] == "0"):
                print("Asiento", chr(ord("A") + i), (j + 1), "disponible")

if __name__ == "__main__":
    asientos =  [   ["X", "0", "X", "0"],
                    ["X", "X", "X", "0"],
                    ["0", "0", "X", "X"]
                ]
    mostrar_disponibilidad(asientos)
    