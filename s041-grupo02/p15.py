def es_primo(N):
    contador = 0
    for i in range(1, N + 1):
        if(N % i == 0):
            contador = contador + 1
    if(contador == 2):
        return True
    else:
        return False

def contar_cifras(N):
    contador = 0
    while(N > 0):
        N = N // 10
        contador = contador + 1
    return contador

def quitar_primer_digito(N):
    cant_cifras = contar_cifras(N)
    return N % (10 ** (cant_cifras - 1))

def es_primo_truncable_izq(N):
    respuesta = True
    while(N > 0 and respuesta == True):
        respuesta = es_primo(N)
        N = quitar_primer_digito(N)
    return respuesta

if __name__ == "__main__":      
    N = 348754
    print(es_primo_truncable_izq(N))



