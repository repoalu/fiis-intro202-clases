N = 73664781
dig_mayor = 0
dig_menor = 9

while(N > 0):
    digito = N % 10
    if(digito > dig_mayor):
        dig_mayor = digito
    if(digito < dig_menor):
        dig_menor = digito
    N = N // 10

print("Menor: ", dig_menor)
print("Mayor: ", dig_mayor)