#0 * 4 ^ 0+ 3 * 4 ^ 1 + 2 * 4 ^ 2 + 0 * 4 ^ 3 + ....
N = 111
b = 2
factor = 1
suma = 0
while(N > 0):
    digito = N % 10
    suma = suma + digito * factor
    factor = factor * b
    N = N // 10
print("El valor convertido es: ", suma)