'''
Implemente una funcion que reciba 2 listas L1 y L2 que representan
placas de vehiculos y la cantidad de infracciones cometidas por
cada vehiculo respectivamente. Su funcion debe mostrar en pantalla
un reporte con la informacion de vehiculos e infracciones.
Ejm:
L1 = ["B8U-349", "N9Y-457"]
L2 = [2, 4]
De las listas anteriores podemos decir que el automovil de placa 
B8U-349 ha tometido 2 infracciones en total.
''' 
def mostrar_reporte(L1, L2):
    for i in range(len(L1)):
        placa = L1[i]
        infracciones = L2[i]
        print("**********************")
        print("Placa:", placa)
        print("Infracciones:", infracciones)
        print("**********************")
    
if __name__ == "__main__":
    L1 = ["B8U-349", "N9Y-457", "H6Y-495"]
    L2 = [2, 4, 2]
    mostrar_reporte(L1, L2)