'''
Implemente una funcion que reciba una lista L y lea de teclado
un valor entero. Solamente si el valor ingresado es mayor que
cero debe agregarlo a la lista.
'''

def agregar_valor(L):
    valor = int(input("Ingrese valor a agregar: "))
    if(valor > 0):
        L.append(valor)

if __name__ == "__main__":
    L = [10, 20, 34, 45, 13]
    agregar_valor(L)
    print(L)