'''
Implemente una funcion que permita reemplazar todas las ocurrencias
del caracter "Z" en una cadena S con un asterisco: "*".
'''
def convertir(S):
    res = ""
    for i in range(len(S)):
        if(S[i].upper() != "Z"):
            res += S[i]
        else:
            res += "*"
    return res
    

if __name__ == "__main__":
    cadena = "ABCZXYZ"
    cadena = convertir(cadena)
    print(cadena)
