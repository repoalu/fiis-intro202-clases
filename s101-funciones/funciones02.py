'''
Implemente una funcion que reciba 3 numeros y permita retornar el
mayor de ellos. Utilice la funcion creada para la pregunta anterior
'''
import funciones01 as f01
def obtener_mayor3(a, b, c):
    res = 0
    mayor = f01.obtener_mayor(a, b)
    res = f01.obtener_mayor(mayor, c)
    return res

if __name__ == "__main__":
    mayor = obtener_mayor3(10, 22, 9)
    print(mayor)