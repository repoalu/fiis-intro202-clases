'''
Implemente una funcion que reciba dos listas L1 y L2 y retorne
una nueva lista con los valores de L1 y L2 que son impares y 
de 3 cifras.
'''

def cumple_condicion(a):
    res = False
    #Impar y de tres cifras
    if(a % 2 != 0 and (a >= 100 and a < 1000)):
        res = True
    return res

def obtener_lista(L1, L2):
    res = []
    for i in range(len(L1)):
        if(cumple_condicion(L1[i]) == True):
            res.append(L1[i])
    
    for i in range(len(L2)):
        if(cumple_condicion(L2[i]) == True):
            res.append(L2[i])
    return res

if __name__ == "__main__":
    L1 = [101, 341, 349, 43, 34, 1, 231]
    L2 = [100, 334, 781, 9, 1200, 45, 311]
    lista = obtener_lista(L1, L2)
    print(lista)