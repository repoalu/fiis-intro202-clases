'''
Implemente una funcion que permita determinar el mayor de dos
numeros a y b dados como dato. En caso los numeros sean iguales
debe mostrar un mensaje
'''

'''
Entrada: a, b (numericos)
Salida: res (sera el valor mayor entre a y b)
'''
def obtener_mayor(a, b):
    res = 0
    if(a > b):
        res = a
    elif(a < b):
        res = b
    else:
        res = a
        print("Numeros son iguales")
    return res

if __name__ == "__main__":
    mayor = obtener_mayor(10, 33)
    print(mayor ** 2)

