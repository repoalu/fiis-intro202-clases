'''
Implemente una funcion que reciba una matriz M y retorne la suma
de elementos de la diagonal principal de dicha matriz
'''

def obtner_suma_diagonal(M):
    res = 0
    for i in range(len(M)):
        res = res + M[i][i]
    return res

if __name__ == "__main__":
    M = [   [1, 4, 5],
            [5, 6, 2],
            [8, 4, 3]
        ]
    suma = obtner_suma_diagonal(M)
    print(suma)