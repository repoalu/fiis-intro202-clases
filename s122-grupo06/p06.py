def sumar_cifras(n):
    #Caso base: un digito
    if(n < 10):
        suma = n
    else:
        #Recursion: Suma = Ultimo digito + suma_digitos(num_restante)
        suma = n % 10 + sumar_cifras(n // 10)
    return suma

def raiz_digital(n):
    res = 0
    #Caso base: una sola cifra
    if(n < 10):
        res = n
    else:
        #Recursion: el resultado es la raiz digital de la suma de cifras de n
        suma = sumar_cifras(n)
        res = raiz_digital(suma)
    return res


print(raiz_digital(147))