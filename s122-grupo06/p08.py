def maximo_aux(L, n):
    #Caso base: un solo elemento
    if(n == 1):
        res = L[0]
    else:
        '''
        El maximo valor es el mayor entre el ultimo elemento y el mayor valor
        entre los elementos restantes.
        '''
        ult_elem = L[n - 1]
        max_restantes = maximo_aux(L, n - 1)
        if(max_restantes > ult_elem):
            res = max_restantes
        else:
            res = ult_elem
    return res

def obtener_maximo(L):
    max = maximo_aux(L, len(L))
    return max

L = [10, 500, 434, 12, 1200]
print(obtener_maximo(L))