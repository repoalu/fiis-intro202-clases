def F(n):
    #Caso base
    if(n < 0):
        print("Valor ingresado es incorrecto")
        res = None
    elif(n == 0):
        res = 19
    else:
        res = 14 * F(n - 1)
    return res

print(F(5))