def potencia(x, n):
    #Caso base: exponente cero
    if(n == 0):
        res = 1
    else:
        #Recursion
        res = x * potencia(x, n - 1)
    return res

print(potencia(5, 4))