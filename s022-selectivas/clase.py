#Estructuras selectivas
nota = 9
participaciones = 5
#En caso tenga mas de 10 participaciones, incremente la nota en 1
if (participaciones > 10):
    nota = nota + 1
print(nota)

#Bloque if-else
nota = 12
participaciones = 3

#En caso tenga mas de 5 participaciones, incremente la nota en 2
#En caso contrario, incremente la nota en 1
if(participaciones > 5):
    nota = nota + 2
else:
    nota = nota + 1
print("Nota final:", nota)

#Bloque selectivo multiple
nota = 13
participaciones = 4
#En caso tenga mas de 10 participaciones, 3 puntos mas
#En caso no cumpla lo anterior, pero tenga mas de 5, 2 puntos mas
#En caso no cumpla lo anterior, pero tenga mas de 3, 1 punto nas
#En caso no cumpla ninguno de los anteriores, 0.5 puntos mas

if(participaciones > 10):
    nota = nota + 3
elif(participaciones > 5):
    nota = nota + 2
elif(participaciones > 3):
    nota = nota + 1
else:
    nota = nota + 0.5

print(nota)
