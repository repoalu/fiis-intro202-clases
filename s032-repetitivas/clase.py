def prueba_while():
    #Bucle while
    n = 3
    while(n < 200):
        print(n)
        n = n + 45        

def prueba_for1():
    #Bucle for
    #1, 2, 3, 4, ...., 39
    for i in range(1, 40): #[1, 40>
        print("Valor: ", i)

def prueba_for2():
    #0, 1, 2, 3, 4, ..., 19
    for i in range(20): #[0, 20>
        print(i)

def prueba_for3():
    #1, 3, 5, 7, 9, ...., 39 
    for i in range(1, 40, 2):
        print(i)

def prueba_for4():
    #20, 18, 16, ..., 2
    for i in range(20, 1, -2):
        print(i)

if __name__ == "__main__":
    prueba_for4()
