'''
Implemente un programa que lea un conjunto de numeros y detenga
la lectura cuando el usuario haya ingresado un valor negativo.
Muestre el mayor valor ingresado por el usuario
'''
n = 0
mayor = 0
while(n >= 0):
    if(n > mayor):
        mayor = n
    n = int(input("Ingrese valor de n: "))
print("Mayor valor leido:", mayor)