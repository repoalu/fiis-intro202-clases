import p02

if __name__ == "__main__":
    texto = "Los alumnos juegan en los pasillos y cantan en pleno sol"
    palabra = input("Ingrese palabra: ")
    palabra = palabra.upper()
    diccionario = p02.contar_palabras(texto)
    print("Texto: ", texto)
    if(palabra in diccionario):
        print("Cantidad de apariciones en el texto:", diccionario[palabra])
    else:
        print("Palabra no esta en el diccionario")
