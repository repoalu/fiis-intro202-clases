def procesar_lanzamientos(datos):
    res = {}
    #Por cada lanzamiento
    for i in range(len(datos)):
        #Obtengo el numero
        numero = datos[i]
        #Si el numero no ha salido previamente -> ¿el numero esta en el diccionario?
        if(numero not in res):
            #Agregar el elemento al diccionario (con una aparicion)
            res[numero] = 1
        #Si el elemento ya ha aparecido
        else:            
            #Incrementar en 1 la cantidad de apariciones
            res[numero] = res[numero] + 1
    return res

def main():
    lanzamientos = (1, 1, 2, 4, 5, 5, 3, 1, 4)
    diccionario = procesar_lanzamientos(lanzamientos)
    print(diccionario)

if __name__ == "__main__":
    main()