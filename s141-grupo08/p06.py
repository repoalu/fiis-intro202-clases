def obtener_poder_equipo(equipo, poderes):
    puntos_equipo = 0
    for i in range(len(equipo)):
        nombre_jugador = equipo[i]
        poder_jugador = poderes[nombre_jugador]
        puntos_equipo = puntos_equipo + poder_jugador
    return puntos_equipo

def mostrar_ganador(equipo1, equipo2, poderes):
    puntos_eq1 = obtener_poder_equipo(equipo1, poderes)
    puntos_eq2 = obtener_poder_equipo(equipo2, poderes)
    print("Puntos equipo1: ", puntos_eq1)
    print("Puntos equipo2: ", puntos_eq2)
    if(puntos_eq1 > puntos_eq2):
        print("Ganador: equipo1")
    elif(puntos_eq1 < puntos_eq2):
        print("Ganador: equipo2")
    else:
        print("Se ha producido un empate")
    

if __name__ == "__main__":
    equipo1  = ["carlos05", "mariana3", "ana19"]
    equipo2  = ["jc05", "pch12", "juan21"]

    poderes = {"jc05" : 35, "juan21" : 20, "pch12": 130,
        "carlos05" : 60, "mariana3" : 50, "ana19" : 15}
    
    mostrar_ganador(equipo1, equipo2, poderes)
