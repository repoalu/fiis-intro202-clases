def obtener_lista(cadena):
    aux = ""
    res = []
    for i in range(len(cadena)):
        if(cadena[i] != " "):
            #Continuo agregando letras
            aux = aux + cadena[i]
        else:
            #La palabra ha terminado
            res.append(aux)
            aux = ""
    #Agregar la ultima palabra
    res.append(aux)
    return res

def contar_palabras(cadena):
    lista = obtener_lista(cadena)
    res = {}
    for i in range(len(lista)):
        #Leemos la palabra
        palabra = lista[i].upper()
        #Verifico si la palabra se encuentra en el diccionario
        if(palabra in res):
            #Si se encuentra incremento el conteo de palabras
            res[palabra] = res[palabra] + 1
        else:
            #Si no se encuentra agrego la palabra con una (1) aparicion
            res[palabra] = 1
    return res



if __name__ == "__main__":
    cadena = "Los alumnos juegan en los pasillos. Los alumnos siguen ..."
    frecuencias = contar_palabras(lista_palabras)
    print(frecuencias)
