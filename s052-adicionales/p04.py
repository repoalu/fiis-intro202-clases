'''
Resuelva el ejercicio anterior utilizando funciones
'''
def es_perfecto(N):
    suma_div = 0
    for i in range(1, N):
        if(N % i == 0):
            suma_div = suma_div + i

    if(N == suma_div):
        return True
    else:
        return False

def imprimir_perfectos(M):
    for N in range(1, M + 1):
        if(es_perfecto(N) == True):
            print(N)

if __name__ == "__main__":
    imprimir_perfectos(700)

