'''
Implemente un programa que, dado un entero N, permita imprimir una estructura matricial
de N x N con numeros consecutivos.
Por ejemplo, N = 3
1 2 3
4 5 6
7 8 9
'''
N = 2
contador = 1
for i in range(1, N + 1):
    for j in range(1, N + 1):
        print(contador, end = " ")
        contador = contador + 1
    print()

