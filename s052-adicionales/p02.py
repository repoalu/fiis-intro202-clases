'''
Un numero perfecto es aquel que es igual a la suma de sus divisores propios.
Implemente un programa en Python que permita determinar si un numero N cumple
con la condicion o no
Ejemplo:
6 = 1, 2, 3
Suma = 6 -> 6 cumple la condicion
'''
N = 6
suma_div = 0
for i in range(1, N):
    if(N % i == 0):
        suma_div = suma_div + i

if(N == suma_div):
    print("Cumple la condicion")
else:
    print("No cumple la condicion")



