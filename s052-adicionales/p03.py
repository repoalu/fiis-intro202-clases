'''
Implemente un programa que, dado un numero entero M, permita obtener la lista de numeros
perfectos en el intervalo [1, M]
'''
M = 300 #[1, 300]
for N in range(1, M + 1):
    suma_div = 0
    for i in range(1, N):
        if(N % i == 0):
            suma_div = suma_div + i
    if(N == suma_div):
        print(N)
