def fibonacci(n):
    res = None
    if(n == 1):
        res = 0
    elif(n == 2):
        res = 1
    else:
        res = fibonacci(n - 1) + fibonacci(n - 2)
    return res

def main():
    for i in range(1, 11):
        print(fibonacci(i), end=" ")

main()