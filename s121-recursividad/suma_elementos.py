'''
Implemente un programa que reciba una lista de valores y devuelva la suma de dichos valores
utilizando un algoritmo recursivo
'''
#lista: Valores originales (iniciales)
#n: Cantidad de valores de la lista a considerar
def sumar_aux(lista, n):
    suma = 0
    if(n > 0):
        #La suma de valores es igual al ultimo elemento + suma del resto de valores
        suma = lista[n - 1] + sumar_aux(lista, n - 1)
    return suma

def sumar(lista):
    return sumar_aux(lista, len(lista))

def main():
    lista = [10, 14, 20, 23, 600]
    res = sumar(lista)
    print(res)

