def factorial(n):
    #Caso Base
    if(n == 0):
        return 1
    else:
        #Recursion
        return n * factorial(n - 1)

def main():
    print(factorial(5))

main()