def agregar(lista, n):
    #Buscar posicion del elemento en la lista ordenada (variable indice)
    indice = -1
    for i in range(len(lista)):
        if(lista[i] > n):
            indice = i
            break
    if(indice == -1):
        indice = len(lista)
    print("Indice: ", indice)
    #Incrementar la longitud de la lista
    lista.append(lista[len(lista) - 1])
    #Desplazar elementos a la derecha
    for i in range(len(lista) - 2, indice - 2, -1):
        lista[i + 1] = lista[i]
    #Colocar el elemento en la posicion correspondiente
    lista[indice] = n


if __name__ == "__main__":
    lista = [2, 10, 23, 34, 55, 68]
    valor_agregar = 50
    agregar(lista, valor_agregar)
    print(lista)