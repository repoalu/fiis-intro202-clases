import p02
def leer_datos():
    continuar = True
    lista = []
    while(continuar == True):
        dato = int(input("Ingrese valor: "))
        if(dato > 0):
            lista.append(dato)
        else:
            continuar = False
    return lista

if __name__ == "__main__":
    lista = leer_datos()
    print("Lista original:", lista)
    sublista = p02.obtener_sublista(lista)
    print("Elementos de 3 cifras: ", sublista)
