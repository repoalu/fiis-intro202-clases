def agregar_progresion(lista, n):
    if(len(lista) > 2):
        #razon = ultimo elemento / penultimo elemento
        razon = lista[len(lista) - 1] / lista[len(lista) - 2]
        print("Razon: ", razon)
        #Verificar si el elemento a insertar cumple la condicion:
        #ultimo_elemento * razon = elemento a insertar
        if(lista[len(lista) - 1] * razon == n):
            lista.append(n)
        else:
            print("Elemento no cumple la condicion")
    else:
        lista.append(n)
    
if __name__ == "__main__":
    lista = [3, 9, 27]
    agregar_progresion(lista, 81)
    print(lista)