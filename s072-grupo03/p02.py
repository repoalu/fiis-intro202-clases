def obtener_sublista(lista):
    res = []
    #Recorrer la lista
    for i in range(len(lista)):
        #Por cada elemento, verificamos si tiene 3 cifras
        if(lista[i] >= 100 and lista[i] < 1000):
            #En caso tenga 3 cifras, agregamos el elemento a la lista de respuesta"
            res.append(lista[i])
    return res
    
if __name__ == "__main__":
    lista = [10, 343, 45, 299, 3495, 211, 349]
    res = obtener_sublista(lista)
    print(res)