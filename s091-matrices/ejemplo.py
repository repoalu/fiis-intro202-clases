'''
Dada una matriz de adyacencias M que representa a un grafo G, implemente un programa
que permita imprimir en pantalla la cantidad de vertices y aristas de G
'''

def imprimir_reporte(M):
    contador = 0
    for i in range(len(M)):
        for j in range(len(M[0])):
            if(M[i][j] == 1):
                #Hay una arista
                contador = contador + 1
    print("Cantidad de aristas: ", contador)
    print("Cantidad de vertices: ", len(M))

if __name__ == "__main__":
    M = [   [0, 1, 0],
            [1, 0, 1],
            [0, 1, 0]
        ]