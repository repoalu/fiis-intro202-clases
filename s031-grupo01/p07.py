#Datos de entrada: salario y antiguedad
salario = float(input("Ingrese salario: "))
antiguedad = int(input("Ingrese antiguedad en anios: "))

if(antiguedad > 5):
    #Incremento de salario del 20%
    salario = 1.2 * salario
elif(antiguedad >= 1):
    #Incremento salarial del 10%
    salario = 1.1 * salario

print("Salario final: ", salario)