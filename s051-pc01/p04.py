n = int(input("Ingrese el valor de n: "))
S = 0
if(n % 2 != 0):
    #n es impar
    for i in range(1, n + 1): #[1, n]
        S = S + (4 * i ** 5 + 3 * i ** 2 + 8)

else:
    #n es par
    for i in range(1, n + 1):
        S = S + (i ** 5 + 7 / 13 * i ** 3 + 5)

print(S)