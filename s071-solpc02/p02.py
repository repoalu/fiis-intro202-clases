def es_cuadrado_perfecto(n):
    res = False
    for i in range(n):
        if(i ** 2 == n):
            res = True
    return res

def calcular_expresion(n):
    cuadrado = es_cuadrado_perfecto(n)
    suma = 0
    if(cuadrado == True):
        #1, 2, 3, ..., n
        for i in range(1, n + 1):
            suma = suma + (6 * i + 12)
    else:
        for i in range(1, n + 1):
            suma = suma + (i ** 2 + 12)
    return suma

if __name__ == "__main__":
    print(calcular_expresion(49))