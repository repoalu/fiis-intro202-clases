n = int(input("Ingrese valor de 'n': "))
print("Cantidad de valores que se leeran: ", n)
lista = []
for i in range(n):
    valor = int(input("Ingrese siguiente valor: "))
    lista.append(valor)

valor_mayor = lista[0]
valor_menor = lista[0]
suma = 0
cantidad = len(lista)
#Ya consideramos los primeros elementos (indice cero)
for i in range(1, cantidad):
    if(lista[i] > valor_mayor):
        valor_mayor = lista[i]    
    elif(lista[i] < valor_menor):
        valor_menor = lista[i]
    suma = suma + lista[i]

print("Mayor: ", valor_mayor)
print("Menor: ", valor_menor)
print("Promedio: ", suma / cantidad)
