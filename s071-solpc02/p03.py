def es_primo(n):
    cant_div = 0
    for i in range(1, n + 1):
        if(n % i == 0):
            cant_div = cant_div + 1
    if(cant_div == 2):
        return True
    else:
        return False

def imprimir_serie_primos(k):
    primos = []
    #Numeros de "k" cifras
    for i in range(10 ** (k - 1), 10 ** k):
        #Evaluamos si el numero es primo
        if(es_primo(i) == True):
            primos.append(i)

    suma = 0
    for i in range(len(primos)):
        print(primos[i], end = " ")
        suma = suma + i
    print()
    print("Suma de valores:", suma)

if __name__ == "__main__":
    imprimir_serie_primos(2)